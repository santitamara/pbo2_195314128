/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prak1B;

/**
 *
 * @author ASUS
 */
public class UKM {
 

    private String namaUnit;
    private mahasiswa Ketua;
    private mahasiswa Sekretaris;
    private Penduduk Anggota[];

    public UKM(String namaUnit, mahasiswa Ketua, mahasiswa Sekretaris, Penduduk[] Anggota) {
        this.namaUnit = namaUnit;
        this.Ketua = Ketua;
        this.Sekretaris = Sekretaris;
        this.Anggota = Anggota;

    }

    public UKM(String namaUnit, mahasiswa Ketua, mahasiswa Sekretaris) {
        this.namaUnit = namaUnit;
        this.Ketua = Ketua;
        this.Sekretaris = Sekretaris;

    }

    public UKM(String namaUnit) {
        this.namaUnit = namaUnit;

    }

    public UKM() {

    }


    public void setNamaUnit(String namaUnit) {
        this.namaUnit = namaUnit;

    }

    public String getNamaUnit() {
        return namaUnit;

    }

    public void setKetua(mahasiswa Ketua) {
        this.Ketua = Ketua;

    }

    public mahasiswa getKetua() {
        return Ketua;
    }

    public void setSekretaris(mahasiswa Sekretaris) {
        this.Sekretaris = Sekretaris;

    }

    public mahasiswa getSekretaris() {
        return Sekretaris;

    }

    public void setAnggota(Penduduk[] Anggota) {
        this.Anggota = Anggota;
    }

    public Penduduk[] getAnggota() {
        return Anggota;

    }

    
}


