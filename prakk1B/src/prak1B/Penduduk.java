/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prak1B;

/**
 *
 * @author ASUS
 */
public abstract class Penduduk {
   String dataNama;
    String dataTTL;
   public Penduduk() {
    
}
   public Penduduk(String dataNama, String dataTTL ){
       this.dataNama = dataNama;
       this.dataTTL = dataTTL;
   }
   public void setDataNama(String dataNama){
       this.dataNama=dataNama;
   }
   public String getDataNama(){
       return dataNama;
   }
   public void setDataTTL(String dataTTL){
       this.dataTTL=dataTTL;
   }
   public String getDataTTL(){
       return dataTTL;
   }
   abstract public double hitungIuran();
     
}
