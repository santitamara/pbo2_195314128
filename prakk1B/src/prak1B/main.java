/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prak1B;

/**
 *
 * @author ASUS
 */

public class main {

    public static void main(String[] args) {
        UKM mapala = new UKM("TEKSAPALA");
        mahasiswa ketua = new mahasiswa( "Santi Tamara","195314128","16/11/01");
        mahasiswa sekretaris = new mahasiswa("Veronika Ria","195314140","20/04/00");

        mapala.setKetua(ketua);
        mapala.setSekretaris(sekretaris);

        mahasiswa anggota1 = new mahasiswa( "Regina Caeli","06/11/01","195314126" );
        mahasiswa anggota2 = new mahasiswa("Angelina Grace", "24/05/01","195314128");
        masyarakat anggota3 = new masyarakat( "Musniyan", "06/05/99","00096348");
        masyarakat anggota4 = new masyarakat("Ariyanto","12/01/98", "00023743" );

        Penduduk anggota[] = new Penduduk[4];
        anggota[0] = anggota1;
        anggota[1] = anggota2;
        anggota[2] = anggota3;
        anggota[3] = anggota4;

        double totalIuran = 0;

        System.out.println("Nama UKM       :" + mapala.getNamaUnit());
        System.out.println("Ketua UKM      :" + ketua.getDataNama());
        System.out.println("NIM            :" + ketua.getNim());
        System.out.println("Tanggal Lahir  :" + ketua.getDataTTL());
        System.out.println();
        System.out.println("Sekretaris UKM :" + sekretaris.getDataNama());
        System.out.println("NIM            :" + sekretaris.getNim());
        System.out.println("Tangggal Lahir :" + sekretaris.getDataTTL());
        System.out.println();

        System.out.println("Daftar Anggota    : \n");
        System.out.println("==============================================================================================");
        System.out.printf("%-5s", "No |");
        System.out.printf("%8s", "NIM");
        System.out.printf("%8s", " | ");
        System.out.printf("%4s", "\t\tNama");
        System.out.printf("\t\t| ");
        System.out.printf("%5s", "Tanggal Lahir");
        System.out.printf(" | ");
        System.out.printf("%10s", "Iuran");
        System.out.printf("\t|\n");
        System.out.printf("---|---------------|-------------|--------------|----------------------------------------|\n");

        for (int i = 0; i < anggota.length; i++) {
            System.out.println(i + 1 + "  |\t" + anggota[i].toString());
            totalIuran += anggota[i].hitungIuran();

            System.out.println("-----------------------------------------------------------------------------------------");
            System.out.printf("\t\t\t\t\t\tTotal Iuran    | Rp " + Math.round(totalIuran));
            System.out.println("\t|");
            System.out.println("========================================================================================");

        }

    }

}
